# YTNetwork

[![CI Status](http://img.shields.io/travis/lixiang_yatang/YTNetwork.svg?style=flat)](https://travis-ci.org/lixiang_yatang/YTNetwork)
[![Version](https://img.shields.io/cocoapods/v/YTNetwork.svg?style=flat)](http://cocoapods.org/pods/YTNetwork)
[![License](https://img.shields.io/cocoapods/l/YTNetwork.svg?style=flat)](http://cocoapods.org/pods/YTNetwork)
[![Platform](https://img.shields.io/cocoapods/p/YTNetwork.svg?style=flat)](http://cocoapods.org/pods/YTNetwork)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YTNetwork is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YTNetwork"
```

## Author

lixiang_yatang, lixiang@yatang.cn

## License

YTNetwork is available under the MIT license. See the LICENSE file for more info.
