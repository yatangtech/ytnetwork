//
//  NSDictionary+YTRequestParamsConvertable.h
//  Pods
//
//  Created by LiXiang on 2017/5/8.
//
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (YTRequestParamsConvertable)

@end

NS_ASSUME_NONNULL_END
