//
//  YTRequest.m
//  YTNetwork
//
//  Created by LiXiang on 2017/5/4.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import "YTRequest.h"
#import "YTNetworkError.h"
#import "YTNetworkAgent.h"

@implementation YTRequest

- (instancetype)init {
    self = [super init];
    if (self) {
        _isMock = NO;
    }
    return self;
}

- (NSString *)baseUrl {
    NSAssert(NO, @"必须设置baseURL");
    return @"";
}

- (NSDictionary<NSString *, id> *)params {
    return [NSDictionary dictionary];
}

- (YTRequestMethod)requestMethod {
    return YTRequestMethodPost;
}

- (YTResponseSerializerType)responseFormat {
    return YTResponseSerializerTypeJSON;
}

- (void)startWithSuccess:(YTRequestSuccessBlock)success failed:(YTRequestFailBlock)failed {
    NSLog(@"=========== start request ==============");
    NSLog(@"参数: %@", self.params);
    self.successCallback = success;
    self.failedCallback = failed;
#if DEBUG
    if (self.isMock) {
        NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"MockNetData"
                                                               ofType:@"bundle"];
        NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
        NSURL *url = [bundle URLForResource:self.mockFileName
                              withExtension:nil];
        if (url == nil) {
            YTNetworkError *error = [YTNetworkError errorWithDomain:YTNeworkErrorDomain code:YTNetworkErrorCodeResponseDataError];
            self.failedCallback(error);
            return;
        }
        NSData *data = [NSData dataWithContentsOfURL:url];
        if (self.responseSerializerType == YTResponseSerializerTypeHTTP) {
            self.responseObject = data;
            NSError *processDataError = nil;
            self.responseObject = [self processResponse:self.responseObject error:&processDataError];
            if (processDataError) {
                if (self.failedCallback) {
                    self.failedCallback([YTNetworkError yt_errorWithError:processDataError]);
                }
            } else {
                if (self.successCallback) {
                    self.successCallback(self.responseObject);
                }
            }
        } else if (self.responseSerializerType == YTResponseSerializerTypeJSON) {
            NSError *error;
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&error];
            if (error) {
                self.failedCallback([YTNetworkError yt_errorWithError:error]);
            } else {
                self.responseObject = dic;
                NSError *processDataError = nil;
                self.responseObject = [self processResponse:self.responseObject error:&processDataError];
                if (processDataError) {
                    if (self.failedCallback) {
                        self.failedCallback([YTNetworkError yt_errorWithError:processDataError]);
                    }
                } else {
                    if (self.successCallback) {
                        self.successCallback(self.responseObject);
                    }
                }
            }
        }
    } else {
        [[YTNetworkAgent sharedAgent] addRequest:self];
    }
#else
    [[YTNetworkAgent sharedAgent] addRequest:self];
#endif
}

- (void)cancel {
    [self.task cancel];
}

- (NSString *)mockFileName {
    return [NSString stringWithFormat:@"%@.json", NSStringFromClass(self.class)];
}

- (BOOL)isMock {
    return NO;
}

- (id)processResponse:(id)responseData error:(NSError **)error {
    NSLog(@"=========== Response Data ==============");
    NSLog(@"%@", responseData);
    return responseData;
}

@end
