//
//  YTNetworkAgent.m
//  YTNetwork
//
//  Created by LiXiang on 2017/5/4.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import "YTNetworkAgent.h"
#import "YTRequest.h"

#if __has_include(<AFNetworking/AFNetworking.h>)
#import <AFNetworking/AFNetworking.h>
#else
#import "AFNetworking.h"
#endif

static NSString * const YTNetworkAgentLockName = @"com.yatang.networkmodule.lock";

@interface YTNetworkAgent()

@property(nonatomic) AFHTTPSessionManager *manager;
@property(nonatomic) NSMutableDictionary <NSNumber *, YTRequest *> *requestsDic;
@property(nonatomic) NSLock *lock;


@end

@implementation YTNetworkAgent

- (instancetype)init {
    self = [super init];
    if (self) {
        _manager = [AFHTTPSessionManager manager];
        _manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithArray:@[@"application/json",
                                                                                   @"text/html",
                                                                                   @"text/json",
                                                                                   @"text/plain",
                                                                                   @"text/javascript",
                                                                                   @"text/xml",
                                                                                   @"image/*"]];
        _manager.requestSerializer.timeoutInterval = 60.f;
        _manager.operationQueue.maxConcurrentOperationCount = 3;
        _lock = [[NSLock alloc] init];
        _lock.name = YTNetworkAgentLockName;
        
        _requestsDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}

+ (instancetype)sharedAgent {
    static YTNetworkAgent *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[YTNetworkAgent alloc] init];
    });
    return instance;
}

- (void)addRequest:(YTRequest *)request {
    NSAssert(request, @"request must not be nil!");
    NSError *error = nil;
    NSURLSessionDataTask *dataTask = [self sessionTaskForRequest:request error:&error];
    request.task = dataTask;
    [self addRequestToDictionary:request];
    if (!error) {
        [dataTask resume];
    } else {
        request.error = error;
    }
}

- (NSURLSessionDataTask *)sessionTaskForRequest:(YTRequest *)request error:(NSError * _Nullable __autoreleasing *)error {
    NSURLSessionDataTask *dataTask;
    AFHTTPRequestSerializer *serializer = [self requestSerializerForRequest:request];
    if (request.cookies) {
        [serializer setValue:request.cookies forHTTPHeaderField:@"Cookie"];
    }
    NSString *urlString = [self URLStringWithBase:request.baseUrl path:request.path];
    switch (request.requestMethod) {
        case YTRequestMethodGet:
            dataTask = [self dataTaskWithHTTPMethod:@"GET"
                                  requestSerializer:serializer
                                          URLString:urlString
                                         parameters:nil
                          constructingBodyWithBlock:request.constructingBodyBlock
                                              error:error];
            break;
        case YTRequestMethodPost:
            dataTask = [self dataTaskWithHTTPMethod:@"POST"
                                  requestSerializer:serializer
                                          URLString:urlString
                                         parameters:request.params
                          constructingBodyWithBlock:request.constructingBodyBlock
                                              error:error];
            break;
        case YTRequestMethodPut:
            dataTask = [self dataTaskWithHTTPMethod:@"PUT"
                                  requestSerializer:serializer
                                          URLString:urlString
                                         parameters:nil
                          constructingBodyWithBlock:request.constructingBodyBlock
                                              error:error];
            break;
        case YTRequestMethodHead:
            dataTask = [self dataTaskWithHTTPMethod:@"HEAD"
                                  requestSerializer:serializer
                                          URLString:urlString
                                         parameters:nil
                          constructingBodyWithBlock:request.constructingBodyBlock
                                              error:error];
            break;
        case YTRequestMethodPatch:
            dataTask = [self dataTaskWithHTTPMethod:@"BATCH"
                                  requestSerializer:serializer
                                          URLString:urlString
                                         parameters:nil
                          constructingBodyWithBlock:request.constructingBodyBlock
                                              error:error];
            break;
        case YTRequestMethodDelete:
            dataTask = [self dataTaskWithHTTPMethod:@"DELETE"
                                  requestSerializer:serializer
                                          URLString:urlString
                                         parameters:nil
                          constructingBodyWithBlock:request.constructingBodyBlock
                                              error:error];
            break;
        default:
            NSAssert(NO, @"Not support current request method!");
            break;
    }
    return dataTask;
}

- (NSURLSessionDataTask *)dataTaskWithHTTPMethod:(NSString *)method
                               requestSerializer:(AFHTTPRequestSerializer *)requestSerializer
                                       URLString:(NSString *)URLString
                                      parameters:(nullable NSDictionary<NSString *, id> *)parameters
                       constructingBodyWithBlock:(nullable void (^)(id <AFMultipartFormData> formData))block
                                           error:(NSError *__autoreleasing  _Nullable * _Nullable)error {
    NSURLRequest *request = nil;
    if (block) {
        request = [requestSerializer multipartFormRequestWithMethod:method
                                                          URLString:URLString
                                                         parameters:parameters
                                          constructingBodyWithBlock:block
                                                              error:error];
    } else {
        request = [requestSerializer requestWithMethod:method
                                             URLString:URLString
                                            parameters:parameters
                                                 error:error];
    }
    __block NSURLSessionDataTask *dataTask = nil;
    dataTask = [self.manager dataTaskWithRequest:request
                                  uploadProgress:nil downloadProgress:nil
                               completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                                   [self handleRequestResult:dataTask responseObject:responseObject error:error];
                               }];
    return dataTask;
}

- (void)handleRequestResult:(NSURLSessionDataTask *)task responseObject:(id _Nullable)responseObject error:(NSError * _Nullable)error {
    [self.lock lock];
    YTRequest *request = self.requestsDic[@(task.taskIdentifier)];
    [self.lock unlock];
    if (error) {
        if (request.failedCallback) {
            request.failedCallback([YTNetworkError yt_errorWithError:error]);
        }
    } else {
        if (request.responseSerializerType == YTResponseSerializerTypeHTTP) {
            NSError *processDataError = nil;
            request.responseObject = [request processResponse:responseObject error:&processDataError];
            if (processDataError) {
                if (request.failedCallback) {
                    request.failedCallback([YTNetworkError yt_errorWithError:processDataError]);
                }
            } else {
                if (request.successCallback) {
                    request.successCallback(request.responseObject);
                }
            }
        } else if (request.responseSerializerType == YTResponseSerializerTypeJSON) {
            NSError *serializationError = nil;
            request.responseObject = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&serializationError];
            if (serializationError) {
                if (request.failedCallback) {
                    request.failedCallback([YTNetworkError yt_errorWithError:serializationError]);
                }
            } else {
                NSError *processDataError = nil;
                request.responseObject = [request processResponse:request.responseObject error:&processDataError];
                if (processDataError) {
                    if (request.failedCallback) {
                        request.failedCallback([YTNetworkError yt_errorWithError:processDataError]);
                    }
                } else {
                    if (request.successCallback) {
                        request.successCallback(request.responseObject);
                    }
                }
            }
        }
    }
}

#pragma mark - 私有方法
- (NSString *)URLStringWithBase:(NSString *)base path:(NSString *)path {
    NSURL *baseURL = [NSURL URLWithString:base];
    NSAssert(baseURL, @"baseURL is invalid!");
    NSURL *url = [NSURL URLWithString:path relativeToURL:baseURL];
    NSAssert(url, @"absolute url is invalid!");
    return [url absoluteString];
}

- (AFHTTPRequestSerializer *)requestSerializerForRequest:(YTRequest *)request {
    switch (request.requestSerializerType) {
        case YTRequestSerializerTypeHTTP:
            return [AFHTTPRequestSerializer serializer];
        case YTRequestSerializerTypeJSON:
            return [AFJSONRequestSerializer serializer];
        default:
            return [AFHTTPRequestSerializer serializer];
    }
}

- (void)addRequestToDictionary:(YTRequest *)request {
    [self.lock lock];
    self.requestsDic[@(request.task.taskIdentifier)] = request;
    [self.lock unlock];
    NSLog(@"Request queue size = %zd", [self.requestsDic count]);
}

- (void)removeRequestFromDictionary:(YTRequest *)request {
    [self.lock lock];
    [self.requestsDic removeObjectForKey:@(request.task.taskIdentifier)];
    NSLog(@"Request queue size = %zd", [self.requestsDic count]);
    [self.lock unlock];
}

@end
