//
//  YTNetworkAgent.h
//  YTNetwork
//
//  Created by LiXiang on 2017/5/4.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YTNetworkError.h"

NS_ASSUME_NONNULL_BEGIN

@class YTRequest;
@interface YTNetworkAgent : NSObject

/**
 单例

 @return YTNetworkAgent
 */
+ (instancetype)sharedAgent;

/**
 添加一个请求的实例

 @param request 请求
 */
- (void)addRequest:(YTRequest *)request;

@end

NS_ASSUME_NONNULL_END
