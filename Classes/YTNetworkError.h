//
//  YTNetworkError.h
//  YTNetwork
//
//  Created by LiXiang on 2017/5/4.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

static NSString * const YTNeworkErrorDomain = @"com.yatang.network";

typedef NS_ENUM(NSInteger, YTNetworkErrorCode) {
    YTNetworkErrorCodeResponseDataError = 3840,
    YTNetworkErrorCodeTimeout = -1001,
    YTNetworkErrorCodeLoseConnect = -1009,
    YTNetworkErrorCodeNetwork = -1004,
    YTNetworkErrorCodeServerInvalid = -1011,
    YTNetworkErrorCodeJSONFormatError = 10000,
};

@interface YTNetworkError : NSError

/**
 通过NSError创建一个YTNetworkError实例

 @param error NSError
 @return YTNetworkError
 */
+ (YTNetworkError *)yt_errorWithError:(NSError *)error;

+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code;

+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code description:(NSString *)description;

@end

NS_ASSUME_NONNULL_END
