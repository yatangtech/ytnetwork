//
//  YTRequest.h
//  YTNetwork
//
//  Created by LiXiang on 2017/5/4.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class YTNetworkError;
@class YTRequest;
@protocol AFMultipartFormData;
@protocol YTRequestParamsConvertable;

typedef NS_ENUM(NSUInteger, YTRequestSerializerType) {
    YTRequestSerializerTypeHTTP,
    YTRequestSerializerTypeJSON,
};

typedef NS_ENUM(NSUInteger, YTResponseSerializerType) {
    YTResponseSerializerTypeHTTP,
    YTResponseSerializerTypeJSON,
};

typedef NS_ENUM(NSUInteger, YTRequestMethod){
    YTRequestMethodGet = 0,
    YTRequestMethodPost,
    YTRequestMethodHead,
    YTRequestMethodPut,
    YTRequestMethodDelete,
    YTRequestMethodPatch
};


typedef void (^YTRequestSuccessBlock) (id responseData);
typedef void (^YTRequestFailBlock) (__kindof NSError *error);
typedef void (^YTConstructingBlock)(id<AFMultipartFormData> formData);

@protocol YTRequestDelegate <NSObject>

- (void)requestWillStart:(YTRequest *)request;

- (void)requestDidFinish:(YTRequest *)request error:(__kindof NSError *)error;

@end

@interface YTRequest : NSObject

/**
 代理
 */
@property(nonatomic) id<YTRequestDelegate> delegate;

/**
 API域名
 */
@property(nonatomic, readonly) NSString *baseUrl;
    
/**
 API接口
 */
@property(nonatomic, readonly) NSString *path;

/**
 请求的参数
 */
@property(nonatomic, nullable, readonly) NSDictionary<NSString *, id> *params;

/**
 Cookies
 */
@property(nonatomic) NSString *cookies;

/**
 NSURLSessionTask
 */
@property(nonatomic) NSURLSessionTask *task;

/**
 返回的原始数据
 */
@property(nonatomic) id responseObject;

/**
 解析过后的JSON数据
 */
@property(nonatomic, nullable) id responseJSONObject;

/**
 构建参数的回调
 */
@property(nonatomic, copy, nullable) YTConstructingBlock constructingBodyBlock;

/**
 成功回调
 */
@property(nonatomic, copy) YTRequestSuccessBlock successCallback;

/**
 失败回调
 */
@property(nonatomic, copy) YTRequestFailBlock failedCallback;

/**
 错误信息
 */
@property(nonatomic, nullable) NSError *error;

/**
 请求的类型
 */
@property(nonatomic, readonly) YTRequestMethod requestMethod;

/**
 请求的数据类型
 */
@property(nonatomic, readonly) YTRequestSerializerType requestSerializerType;

/**
 服务器返回的数据类型
 */
@property(nonatomic, readonly) YTResponseSerializerType responseSerializerType;

/**
 模拟请求数据的文件路径
 */
@property(nonatomic, readonly) NSString *mockFileName;

/**
 是否模拟请求数据
 */
@property(nonatomic) BOOL isMock;

/**
 开始请求

 @param success 成功的回调
 @param failed 失败的回调
 */
- (void)startWithSuccess:(YTRequestSuccessBlock)success failed:(YTRequestFailBlock)failed;

/**
 取消请求
 */
- (void)cancel;

/**
 处理服务器返回的数据

 @param responseData 响应的数据
 */
- (id)processResponse:(id)responseData error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
