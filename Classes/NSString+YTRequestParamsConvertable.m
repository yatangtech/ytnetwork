//
//  NSString+YTRequestParamsConvertable.m
//  Pods
//
//  Created by LiXiang on 2017/5/8.
//
//

#import "NSString+YTRequestParamsConvertable.h"
#import "YTRequestParamsConvertable.h"

@implementation NSString (YTRequestParamsConvertable)

- (NSString *)convertToParamsString {
    return self;
}

- (id<YTRequestParamsConvertable>)addParams:(id<YTRequestParamsConvertable>)newParams {
    return (id<YTRequestParamsConvertable>)[NSString stringWithFormat:@"%@%@", self, (NSString *)newParams];
}

@end
