//
//  YTNetworkError.m
//  YTNetwork
//
//  Created by LiXiang on 2017/5/4.
//  Copyright © 2017年 Ya Tang. All rights reserved.
//

#import "YTNetworkError.h"

@implementation YTNetworkError

+ (YTNetworkError *)yt_errorWithError:(NSError *)error {
    YTNetworkError *yt_error = [[YTNetworkError alloc] initWithDomain:YTNeworkErrorDomain code:error.code userInfo:error.userInfo];
    return yt_error;
}

- (NSString *)localizedDescription {
    switch (self.code) {
        case YTNetworkErrorCodeResponseDataError:
            return @"服务器返回数据有误!";
        case YTNetworkErrorCodeTimeout:
            return @"请求超时!";
        case YTNetworkErrorCodeLoseConnect:
            return @"网络连接失败!";
        case YTNetworkErrorCodeNetwork:
            return @"网络连接失败!";
        case YTNetworkErrorCodeServerInvalid:
            return @"服务器异常!";
        default:
            return [super localizedDescription];
    }
}

+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code {
    domain = domain ?: @"";
    return [YTNetworkError errorWithDomain:domain code:code userInfo:nil];
}


+ (instancetype)errorWithDomain:(NSString *)domain code:(NSInteger)code description:(NSString *)description {
    domain = domain ?: @"";
    description = description ?: @"";
    return [YTNetworkError errorWithDomain:domain code:code userInfo:@{NSLocalizedDescriptionKey : description}];
}

@end
