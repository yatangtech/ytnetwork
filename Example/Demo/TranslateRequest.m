//
//  TranslateRequest.m
//  YTNetwork-Demo
//
//  Created by LiXiang on 2017/5/6.
//  Copyright © 2017年 lixiang_yatang. All rights reserved.
//

#import "TranslateRequest.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (md5)
static NSString * const kBaiduApiId = @"20160604000022762";
static NSString * const kBaiduApiSecret = @"FW77YYqkCK6KJ5F0rrBu";
- (NSString *)md5 {
    const char *cStr = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), result );
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

@end

@implementation TranslateRequest
- (NSString *)path {
    return @"translate";
}

- (NSDictionary<NSString *, id> *)params {
    NSString *q = @"hello world!";
    NSString *salt = [NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]];
    NSString *sign = [[NSString stringWithFormat:@"%@%@%@%@", kBaiduApiId, q, salt, kBaiduApiSecret] md5];
    return @{@"appid": kBaiduApiId, @"q": q, @"from": @"en", @"to": @"zh", @"salt": salt, @"sign": sign};
}


@end
