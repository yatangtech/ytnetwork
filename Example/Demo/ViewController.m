//
//  ViewController.m
//  Demo
//
//  Created by LiXiang on 2017/5/6.
//  Copyright © 2017年 lixiang_yatang. All rights reserved.
//

#import "ViewController.h"
#import "TranslateRequest.h"

@interface ViewController ()

@property(nonatomic) TranslateRequest *translateRequest;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.translateRequest = [[TranslateRequest alloc] init];
    [self.translateRequest startWithSuccess:^(__kindof YTRequest * _Nonnull request) {
        NSData *data = request.responseObject;
        NSData *data2 = request.responseJSONObject;
    } failed:^(__kindof NSError * _Nonnull error) {
        NSLog([error localizedDescription]);
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
