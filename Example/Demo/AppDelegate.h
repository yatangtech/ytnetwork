//
//  AppDelegate.h
//  Demo
//
//  Created by LiXiang on 2017/5/6.
//  Copyright © 2017年 lixiang_yatang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

