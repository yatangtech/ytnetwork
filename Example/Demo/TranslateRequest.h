//
//  TranslateRequest.h
//  YTNetwork-Demo
//
//  Created by LiXiang on 2017/5/6.
//  Copyright © 2017年 lixiang_yatang. All rights reserved.
//

#import "BaseRequest.h"

@interface NSString (md5)

- (NSString *)md5;

@end

@interface TranslateRequest : BaseRequest

@end
